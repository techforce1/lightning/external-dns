# External DNS lightning talk

## Mooi spul

- [Wat is dns](https://www.cloudflare.com/learning/dns/what-is-dns/)
- [Stichting NLnet Labs - Open Source Nederlands DNS paradijs](https://www.nlnetlabs.nl/)
- [External DNS Docs](https://kubernetes-sigs.github.io/external-dns/v0.14.2/)
- [Screencast uit de presentatie](https://asciinema.org/a/YmxaU3pwP8mJrQQZBiuMmcfjx)
- [Hoe werkt DNSSEC](https://www.cloudflare.com/dns/dnssec/how-dnssec-works/)
- [DNSSEC verplichting van de overheid](https://www.forumstandaardisatie.nl/open-standaarden/dnssec)
- [Azure heeft nog steeds geen DNSSEC ondersteuning (20 jaar oud) 🤡🤡](https://feedback.azure.com/d365community/idea/d403899e-8526-ec11-b6e6-000d3a4f0789)
- [Hoe maakt SIDN dns beter](https://tweakers.net/plan/2698/hoe-sidn-met-dns-anycast-het-nl-domein-steeds-robuuster-maakt.html)
- [Kubernetes - External DNS repo](https://github.com/kubernetes-sigs/external-dns/tree/master?tab=readme-ov-file#externaldns)
- [External DNS ontwikkelingen](https://github.com/kubernetes-sigs/external-dns/tree/master?tab=readme-ov-file#new-providers)
- [Asciinema](https://asciinema.org)
- [DNS Certification Authority Authorization Wiki](https://en.wikipedia.org/wiki/DNS_Certification_Authority_Authorization)
- [Hoe integreer je External DNS veilig in Azure](https://cloudchronicles.blog/blog/ExternalDNS-integration-with-Azure-DNS-using-workload-identity/)
- [External DNS helm chart](https://github.com/kubernetes-sigs/external-dns/tree/master/charts/external-dns#readme)
- [External DNS voorbeelden/tutorials](https://github.com/kubernetes-sigs/external-dns/tree/master/docs/tutorials)
- [OpenShift/RedHat/IBM External DNS Operator](https://github.com/openshift/external-dns-operator)
